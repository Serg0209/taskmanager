<?php
namespace TaskBundle\Security;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Doctrine\ORM\EntityRepository;
use MeetingBundle\Entity\User;

class UserProvider extends EntityRepository implements UserProviderInterface {


    public $em;

    public function __construct(EntityManager $em){
        $this->em = $em;
    }

    /**
     * @throws UsernameNotFoundException if the user is not found
     * @param string $username The username
     *
     * @return UserInterface
     */
    function loadUserByUsername($username) {
        $user = $this->em->getRepository('TaskBundle:User')->findOneByUsername($username);

        if(empty($user)){
            throw new UsernameNotFoundException('Could not find user. Sorry!');
        }
        $this->user = $user;
        return $user;
    }

    /**
     * @throws UnsupportedUserException if the account is not supported
     * @param UserInterface $user
     *
     * @return UserInterface
     */
    function refreshUser(UserInterface $user) {
        return $user;
    }

    /**
     * @param string $class
     *
     * @return Boolean
     */
    function supportsClass($class) {
        return $class === 'TaskBundle\Entity\User';
    }
}