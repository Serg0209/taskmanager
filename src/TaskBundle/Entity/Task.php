<?php
/**
 * Created by PhpStorm.
 * User: serg
 * Date: 12.10.15
 * Time: 16:11
 */

namespace TaskBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 *
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="TaskBundle\Repository\TaskRepository")
 * @ORM\Table(name="tasks")
 * @ORM\HasLifecycleCallbacks
 */
class Task {
    const STATUS_NEW = 1;
    const STATUS_DOING = 2;
    const STATUS_DONE = 3;


    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Поле не должно быть пустым")
     * @Assert\Length(
     *      min = 5,
     *      max = 50,
     *      minMessage = "Название  должно быть длинее 5 символов",
     *      maxMessage = "Название  должно быть не более 50 символов"
     * )
     */
    protected $title;
    /**
     * @Assert\NotBlank(message="Поле не должно быть пустым")
     * @ORM\Column(type="string")
     */
    protected $description;
    /**
     * @ORM\Column(type="integer")
     */
    protected $status = self::STATUS_NEW;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="creator", referencedColumnName="id")
     **/
    protected $creator;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $created;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Task
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Task
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Task
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

//    /**
//     * Set creator
//     *
//     * @param \TaskBundle\Entity\User $creator
//     * @return Task
//     */
//    public function setCreator(\TaskBundle\Entity\User $creator = null)
//    {
//        $this->creator = $creator;
//        return $this;
//    }

    /**
     * Get creator
     *
     * @return \TaskBundle\Entity\User 
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**

     * @ORM\PrePersist()
     * @param \DateTime $created
     * @return Task
     */
    public function setCreated()
    {
        $this->created = new \DateTime();

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }
}
