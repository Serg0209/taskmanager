<?php

namespace TaskBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use TaskBundle\Entity\User;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="board")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $tasks = $em->getRepository("TaskBundle:Task")->findAll();

        return $this->render('@Task/Default/index.html.twig',
            [
                'tasks' => $tasks,
            ]
        );
    }
    /**
     * @Route("/change_status/{status}/{id}", name="change_status")
     */
    public function newTaskAction($status, $id)
    {
         $em = $this->getDoctrine()->getManager();
         $task = $em->getRepository("TaskBundle:Task")->findOneById($id);
         $task->setStatus($status);
         $em->flush();
        return $this->redirect($this->generateUrl("board"));
    }
}
